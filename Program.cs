﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGameProject
{
    static class Program
    {
        static void Main(string[] args)
        {
            int len = 0;
            int width = 0;
            int countOfSteps = 0;
            bool flag = false;
           
            Console.SetWindowPosition(0, 0);
            Console.WriteLine("Welcome to \"MyGame\"");
            while (len < 20 || width < 30 || countOfSteps < 20 || !flag)
            {
                try
                {
                    Console.WriteLine("Enter size for the gameboard.");
                    Console.WriteLine("Minimum lenght should be 20.\nMinimum width should be 30.\nMinimum number of steps should be 20.\n");
                    Console.Write("Lenght is : ");
                    len = int.Parse(Console.ReadLine());
                    Console.Write("Width is : ");
                    width = int.Parse(Console.ReadLine());
                    Console.WriteLine("Enter number of steps");
                    Console.Write("Number of steps is : ");
                    countOfSteps = int.Parse(Console.ReadLine());
                    int maxWidthOfScreen = Console.WindowWidth * len / 25;
                    int maxHeightOfScreen = Console.WindowHeight * width / 20;
                    Console.SetWindowSize(maxWidthOfScreen, maxHeightOfScreen);
                    flag = true;
                    Console.WriteLine();
                }                
                catch (FormatException)
                {
                    Console.WriteLine("\nIncorrect number.\n");
                }
                catch(OverflowException)
                {
                    Console.WriteLine("\nNumber is too big.\n");
                }
                catch (ArgumentOutOfRangeException)
                {
                    flag = false;
                    Console.WriteLine("\nSize of board is too big.\n");
                }
            }  

            var game = new MyGame(len, width, countOfSteps);
            game.Start();

            Console.ReadLine();
        }
    }
}
