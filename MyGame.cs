﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGameProject
{
    class MyGame
    {
        private readonly int lenghtBoard;
        private readonly int widthBoard;
        private readonly int countOfSteps;
        private char[,] board;
        private readonly string playerOne = "One";
        private readonly string playerTwo = "Two";

        public MyGame(int lenghtBoard, int widthBoard, int countOfSteps)
        {
            this.lenghtBoard = lenghtBoard;
            this.widthBoard = widthBoard;
            this.countOfSteps = countOfSteps;

            Init();
        }

        public void Start()
        {
            var playerOneStep = countOfSteps;
            var playerTwoStep = countOfSteps;
            var currentStep = 0;
           
            while (playerTwoStep > 0 && IsCheckingBoard())
            { 
                if (currentStep % 2 == 0)
                {
                    playerOneStep = GetSteps(playerOneStep, playerOne, '*');
                }
                else
                {
                    playerTwoStep = GetSteps(playerTwoStep, playerTwo, '+');
                }

                currentStep++;
            }

            GetWinner();
        }

        private void Init()
        {
            board = new char[widthBoard, lenghtBoard];
            Console.WriteLine("\nThe game was starting.\n");
            Console.WriteLine($"Lenght: {this.lenghtBoard}\tWidth: {this.widthBoard}\tSteps: {countOfSteps}\n");
            FillBoard(0, 0, lenghtBoard, widthBoard, 'X');
            PrintBoard();

        }

        private bool IsCheckingBoard()
        {
            var check = false;
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[i, j] == 'X')
                    {
                        check = true;
                    }                    
                }
            }

            return check;
        }

        private void GetWinner()
        {
            var countPlr1 = 0;
            var countPlr2 = 0;
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[i, j] == '*')
                    {
                        countPlr1++;
                    }
                    else if (board[i, j] == '+')
                    {
                        countPlr2++;
                    }
                }
            }

            if (countPlr1 > countPlr2)
            {
                Console.WriteLine($"Player {playerOne} won!!!");
            }
            else if (countPlr2 > countPlr1)
            {
                Console.WriteLine($"Player {playerTwo} won!!!");
            }
            else
            {
                Console.WriteLine("Draw!");
            }
        }

        private int GetSteps(int currentPlayerStep, string playerName, char symbol)
        {
            var random = new Random();
            var firstRoll = RollTheDice(random);
            var secondRoll = RollTheDice(random);
            Console.WriteLine($"First roll: {firstRoll}\t Second roll: {secondRoll}.");

            GetXY(playerName, out int x, out int y);

            if (!IsCheckFreePlace(firstRoll, secondRoll))
            {
                firstRoll = RollTheDice(random);
                secondRoll = RollTheDice(random);
                Console.WriteLine($"First roll: {firstRoll}\t Second roll: {secondRoll}.");
                if (!IsCheckFreePlace(firstRoll, secondRoll))
                {
                    return currentPlayerStep - 1;
                }
            }

            while (!IsChecking(x, y, x + firstRoll, y + secondRoll))
            {
                Console.WriteLine("Wrong move! Try Again!");
                GetXY(playerName, out x, out y);
            }
            FillBoard(x - 1, y - 1, x + firstRoll - 1, y + secondRoll - 1, symbol);
            PrintBoard();
            currentPlayerStep--;

            return currentPlayerStep;
        }

        private static void GetXY(string playerName, out int x, out int y)
        {
            x = 0;
            y = 0;
            bool flag = false;
            while (!flag)
            { 
                try
                {
                    Console.WriteLine($"\nPlayer {playerName} makes a move.");
                    Console.WriteLine("Enter your start point.");
                    Console.Write("X= ");
                    x = int.Parse(Console.ReadLine());
                    Console.Write("Y= ");
                    y = int.Parse(Console.ReadLine());
                    flag = true;                    
                }
                catch (FormatException)
                {
                    flag = false;
                    Console.WriteLine("\nIncorrect number.\n");
                }
                catch (OverflowException)
                {
                    flag = false;
                    Console.WriteLine("\nNumber is very big.\n");
                }

                if (x <= 0 || y <= 0)
                {
                    flag = false;
                    Console.WriteLine("\nNumber should be more than zero.\n");

                }
            }                       

        }

        private int RollTheDice(Random rnd) => rnd.Next(1, 7);

        private bool IsCheckFreePlace(int firstRoll, int secondRol)
        {
            var check = false;
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (board[i, j] == 'X')
                    {
                        check = IsChecking(i, j, i + firstRoll, j + secondRol);
                    }

                    if(check)
                    {
                        return true;
                    }
                }
            }

            return check;
        }

        private bool IsChecking(int startX, int startY, int endX, int endY)
        {
            if (endX >= lenghtBoard || endY >= widthBoard)
            {
                return false;
            }

            for (int i = startY; i < endY; i++)
            {
                for (int j = startX; j < endX; j++)
                {
                    if (board[i, j] != 'X')
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private void FillBoard(int startX, int startY, int endX, int endY, char symbol)
        {
            for (int i = startY; i < endY; i++)
            {
                for (int j = startX; j < endX; j++)
                {
                    board[i, j] = symbol;
                }
            }
        }

        private void PrintBoard()
        {
            Console.Write("  ");
            for (int i = 0; i < board.GetLength(1); i++)
            {
                Console.Write($"{i + 1,3}");
            }
            Console.WriteLine();
            for (int i = 0; i < board.GetLength(0); i++)
            {
                Console.Write($"{i + 1,2}");

                for (int j = 0; j < board.GetLength(1); j++)
                {
                    Console.Write($"{board[i, j], 3}");
                }

                Console.WriteLine();
            }

            Console.WriteLine();
        }


    }
}
